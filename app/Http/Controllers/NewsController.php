<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;

class NewsController extends Controller
{
   public function index()
   {
    
        $news = News::all();
        return view('news.findAll', compact('news'));
    
   }
}
